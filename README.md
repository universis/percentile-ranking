# @universis/percentile-ranking

Percentile ranking is a vital part of any student information system. It indicates how well a student performed in
comparison to the students in the specific norm group, for example, in the same grade, class, or graduation event.
This package implements two ways of calculating the rank, one where all grades have the same coefficient and another
where the equal grades have a coefficient of 0.5. 

## Usage
This can be used with two ways: one with static functions and another with methods.

### Static functions
```typescript
import {Grades, PercentileRanking} from '@universis/percentile-ranking';
let grades: Array<Grades> = [
    {
        student: 1234,
        grade: 0.5
    },
    {
        student: 2345,
        grade: 0.2
    },
    {
        student: 3456,
        grade: 0.1
    }
];
PercentileRanking.simplePercentileCalculation(); // 1, 0.66, 0.33
PercentileRanking.complexPercentileCalculation(); // 0.833, 0.5, 0.166
```

To calculate the rank of a single item, use the following
```typescript
import {Grades, PercentileRanking} from '@universis/percentile-ranking';
let grades: Array<Grades> = [
    {
        student: 1234,
        grade: 0.5
    },
    {
        student: 2345,
        grade: 0.2
    },
    {
        student: 3456,
        grade: 0.1
    }
];
PercentileRanking.calculateSingleSimpleRank(grades, grades[0]); // 1
PercentileRanking.calculateSingleComplexRank(grades, grades[0]); // 0.833
```

### Class methods
```typescript
import {Grades, PercentileRanking} from '@universis/percentile-ranking';
let grades: Array<Grades> = [
    {
        student: 1234,
        grade: 0.5
    },
    {
        student: 2345,
        grade: 0.2
    },
    {
        student: 3456,
        grade: 0.1
    }
];
let percentileRanking = new PercentileRanking(grades);
percentileRanking.simplePercentileCalculation(); // 1, 0.66, 0.33
percentileRanking.complexPercentileCalculation(); // 0.833, 0.5, 0.166
```

To calculate the rank of a single item, use the following
```typescript
import {Grades, PercentileRanking} from '@universis/percentile-ranking';
let grades: Array<Grades> = [
    {
        student: 1234,
        grade: 0.5
    },
    {
        student: 2345,
        grade: 0.2
    },
    {
        student: 3456,
        grade: 0.1
    }
];
let percentileRanking = new PercentileRanking(grades);
percentileRanking.calculateSingleSimpleRank(grades[0]); // 1
percentileRanking.calculateSingleComplexRank(grades[0]); // 0.833

```


Note that the examples use a quite small dataset. To get more accurate rankings, make sure that you have a 
sufficiently large dataset.
