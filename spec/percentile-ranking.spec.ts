import {Grades, PercentileRanking} from '../src';

describe('PercentileRanking', () => {
    let grades: Array<Grades>;
    let expectedGrades: Array<Grades>;
    let expectedComplexGrades: Array<Grades>;
    beforeEach(() => {
        grades = [
            {
                student: 1,
                grade: 7
            },
            {
                student: 2,
                grade: 5
            },
            {
                student: 3,
                grade: 6
            },
            {
                student: 4,
                grade: 8
            },
            {
                student: 5,
                grade: 5
            },
            {
                student: 6,
                grade: 6
            },
            {
                student: 7,
                grade: 7
            },
            {
                student: 8,
                grade: 6
            },
            {
                student: 9,
                grade: 9
            },
            {
                student: 10,
                grade: 7
            }
        ];
        expectedGrades = [
            {
                student: 1,
                grade: 7,
                percentileRank: 0.8
            },
            {
                student: 2,
                grade: 5,
                percentileRank: 0.2
            },
            {
                student: 3,
                grade: 6,
                percentileRank: 0.5
            },
            {
                student: 4,
                grade: 8,
                percentileRank: 0.9
            },
            {
                student: 5,
                grade: 5,
                percentileRank: 0.2
            },
            {
                student: 6,
                grade: 6,
                percentileRank: 0.5
            },
            {
                student: 7,
                grade: 7,
                percentileRank: 0.8
            },
            {
                student: 8,
                grade: 6,
                percentileRank: 0.5
            },
            {
                student: 9,
                grade: 9,
                percentileRank: 1
            },
            {
                student: 10,
                grade: 7,
                percentileRank: 0.8
            }
        ];
        expectedComplexGrades = [
            {
                student: 1,
                grade: 7,
                percentileRank: 0.65
            },
            {
                student: 2,
                grade: 5,
                percentileRank: 0.1
            },
            {
                student: 3,
                grade: 6,
                percentileRank: 0.35
            },
            {
                student: 4,
                grade: 8,
                percentileRank: 0.85
            },
            {
                student: 5,
                grade: 5,
                percentileRank: 0.1
            },
            {
                student: 6,
                grade: 6,
                percentileRank: 0.35
            },
            {
                student: 7,
                grade: 7,
                percentileRank: 0.65
            },
            {
                student: 8,
                grade: 6,
                percentileRank: 0.35
            },
            {
                student: 9,
                grade: 9,
                percentileRank: 0.95
            },
            {
                student: 10,
                grade: 7,
                percentileRank: 0.65
            }
        ];
    })

    it('should calculate simple percentile ranks of', () => {
        let percentileRanking = new PercentileRanking(grades);
        percentileRanking.simplePercentileCalculation();
        expect(percentileRanking.grades.map(x => x.grade)).toEqual(expectedGrades.map(x => x.grade));
    });

    it('should calculate complex percentile ranks of', () => {
        let percentileRanking = new PercentileRanking(grades);
        percentileRanking.complexPercentileCalculation();
        expect(percentileRanking.grades.map(x => x.grade)).toEqual(expectedComplexGrades.map(x => x.grade));
    });

    it('should calculate simple percentile ranks of', () => {
        let percentileRanking = new PercentileRanking(grades);
        percentileRanking.simplePercentileCalculation();
        let ranks = [...percentileRanking.grades.map(x => x.grade), ...expectedGrades.map(x => x.grade)];
        expect(percentileRanking.grades.length).toEqual(expectedGrades.map(x => x.grade).length);
        let len = ranks.length / 2;
        for (let i = 0; i < len; i++) {
            expect(ranks[i]).toEqual(ranks[i + len]);
        }
    });

    it('should calculate complex percentile ranks of', () => {
        let percentileRanking = new PercentileRanking(grades);
        percentileRanking.complexPercentileCalculation();
        let ranks = [...percentileRanking.grades.map(x => x.grade), ...expectedComplexGrades.map(x => x.grade)];
        expect(percentileRanking.grades.length).toEqual(expectedComplexGrades.map(x => x.grade).length);
        let len = ranks.length / 2;
        for (let i = 0; i < len; i++) {
            expect(ranks[i]).toEqual(ranks[i + len]);
        }
    });

    it('should calculate simple percentile ranks using static function', () => {
        PercentileRanking.simplePercentileCalculation(grades);
        expect(grades.map(x => x.grade)).toEqual(expectedGrades.map(x => x.grade));
    });

    it('should use static function for simple percentile calculation', () => {
        PercentileRanking.simplePercentileCalculation(grades);
        let ranks = [...grades.map(x => x.grade), ...expectedGrades.map(x => x.grade)];
        expect(grades.length).toEqual(expectedGrades.map(x => x.grade).length);
        let len = ranks.length / 2;
        for (let i = 0; i < len; i++) {
            expect(ranks[i]).toEqual(ranks[i + len]);
        }
    });

    it('should calculate complex percentile ranks using static function', () => {
        PercentileRanking.complexPercentileCalculation(grades);
        expect(grades.map(x => x.grade)).toEqual(expectedComplexGrades.map(x => x.grade));
    });

    it('should use static function for complex percentile calculation', () => {
        PercentileRanking.complexPercentileCalculation(grades);
        let ranks = [...grades.map(x => x.grade), ...expectedComplexGrades.map(x => x.grade)];
        expect(grades.length).toEqual(expectedComplexGrades.map(x => x.grade).length);
        let len = ranks.length / 2;
        for (let i = 0; i < len; i++) {
            expect(ranks[i]).toEqual(ranks[i + len]);
        }
    });

    it('should calculate the simple rank of a single grade that exists in initial grades array', () => {
        let percentileRanking = new PercentileRanking(grades);
        percentileRanking.calculateSingleSimpleRank(grades[0]);
        expect(grades[0]).toEqual(expectedGrades[0]);
        expect(grades[0].percentileRank).toEqual(expectedGrades[0].percentileRank);
    });

    it('should calculate the simple rank of a single grade that does not exist in initial grades array', () => {
        let percentileRanking = new PercentileRanking(grades);
        let randomGrade: Grades = {
            student: 1234,
            grade: 8.5
        };
        percentileRanking.calculateSingleSimpleRank(randomGrade);
        expect(parseFloat(randomGrade.percentileRank.toFixed(2))).toEqual(0.91)
    });

    it('should calculate the complex rank of a single grade that exists in initial grades array', () => {
        let percentileRanking = new PercentileRanking(grades);
        percentileRanking.calculateSingleComplexRank(grades[0]);
        expect(grades[0]).toEqual(expectedComplexGrades[0]);
        expect(grades[0].percentileRank).toEqual(expectedComplexGrades[0].percentileRank);
    });

    it('should calculate the complex rank of a single grade that does not exist in initial grades array', () => {
        let percentileRanking = new PercentileRanking(grades);
        let randomGrade: Grades = {
            student: 1234,
            grade: 8.5
        };
        percentileRanking.calculateSingleComplexRank(randomGrade);
        expect(parseFloat(randomGrade.percentileRank.toFixed(2))).toEqual(0.86)
    });

    it('should calculate the simple rank of a single grade that exists in initial grades array using static method', () => {
        PercentileRanking.calculateSingleSimpleRank(grades, grades[0]);
        expect(grades[0]).toEqual(expectedGrades[0]);
        expect(grades[0].percentileRank).toEqual(expectedGrades[0].percentileRank);
    });

    it('should calculate the simple rank of a single grade that does not exist in initial grades array using static method', () => {
        let randomGrade: Grades = {
            student: 1234,
            grade: 8.5
        };
        PercentileRanking.calculateSingleSimpleRank(grades, randomGrade);
        expect(parseFloat(randomGrade.percentileRank.toFixed(2))).toEqual(0.91)
    });

    it('should calculate the complex rank of a single grade that exists in initial grades array using static method', () => {
        PercentileRanking.calculateSingleComplexRank(grades, grades[0]);
        expect(grades[0]).toEqual(expectedComplexGrades[0]);
        expect(grades[0].percentileRank).toEqual(expectedComplexGrades[0].percentileRank);
    });

    it('should calculate the complex rank of a single grade that does not exist in initial grades array', () => {
        let randomGrade: Grades = {
            student: 1234,
            grade: 8.5
        };
        PercentileRanking.calculateSingleComplexRank(grades, randomGrade);
        expect(parseFloat(randomGrade.percentileRank.toFixed(2))).toEqual(0.86)
    });

    it('should, having called calculateSingleSimpleRank, have populated the map', () => {
       let percentileRank = new PercentileRanking(grades);
       percentileRank.calculateSingleSimpleRank(grades[0]);
       expect(percentileRank._gradeRank.size).toEqual(1);
       expect(percentileRank._gradeRank.has(grades[0].grade)).toBeTruthy();
       expect(percentileRank._gradeRank.get(grades[0].grade)).toEqual(0.8)
    });

    it('should, having called static calculateSingleSimpleRank, have populated the map', () => {
        let map: Map<number, number> = new Map<number, number>();
        PercentileRanking.calculateSingleSimpleRank(grades,grades[0], map);
        expect(map.size).toEqual(1);
        expect(map.has(grades[0].grade)).toBeTruthy();
        expect(map.get(grades[0].grade)).toEqual(0.8)
    });

});
